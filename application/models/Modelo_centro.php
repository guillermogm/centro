<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Modelo_centro
 *
 * @author jbarrachinab
 */
class Modelo_centro extends CI_Model {
    //put your code here
    public function get_grupos() {
        $sql = <<< SQL
          SELECT id, codigo, nombre 
              FROM grupos                 
SQL;
        $consulta = $this->db->query($sql);
        return $consulta->result();
    }
    
    /*
     * $grupo se pasa el código del grupo 2CFMR o similar
     */
    public function get_alumnos($grupo) {
        $sql = <<< SQL
          SELECT alumnos.NIA, apellido1, apellido2, nombre, email, fecha_nac, nif, matricula.grupo 
            FROM alumnos 
            LEFT JOIN matricula ON alumnos.NIA = matricula.NIA
            WHERE  matricula.grupo = ?                
SQL;
        $consulta = $this->db->query($sql,[$grupo]);
        return $consulta->result();
    }
}
