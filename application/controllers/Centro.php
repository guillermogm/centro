<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Centro
 *
 * @author jbarrachinab
 */
class Centro extends CI_Controller{
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
        $this->load->model('modelo_centro');
        
    }  

    public function index() {
        $data['titulo'] = 'Listado de Grupos';
        $data['grupos'] = $this->modelo_centro->get_grupos();
        $this->load->view('muestra_grupos',$data);               
        /*        
        echo "<pre>";
        print_r($data['grupos'][3]->nombre);
        echo "</pre>";
         */
    }
    
    public function alumnos($id_grupo){
        $data['titulo'] = 'Listado de Alumnos';
        $data['alumnos'] = $this->modelo_centro->get_alumnos($id_grupo);
        $this->load->view('muestra_alumnos',$data);              
        /*echo "<pre>";
        print_r($data['alumnos']);
        echo "</pre>";*/
         
    }   
}
