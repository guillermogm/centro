<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/datatables.min.css"/>
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/datatables.min.js"></script>
        <title>
            <?php echo $titulo; ?>
        </title>
    </head>
    <body>
        <div class="container">
            <h2 class="text-primary"><?php echo $titulo; ?></h2>
            <table id="mitabla" class="table table-striped">
                <thead>
                    <tr>
                        <th>nº</th>
                        <th>código</th>
                        <th>nombre</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($grupos as $grupo) { ?>
                    <tr>
                        <td>
                            <?php echo $grupo->id; ?>
                        </td>
                        <td>
                            <a href="<?php echo site_url('centro/alumnos/'.$grupo->codigo); ?>">
                            <?php echo $grupo->codigo; ?>
                            </a>
                        </td>
                        <td>
                            <?php echo $grupo->nombre; ?>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <script type="text/javascript">
            $(document).ready( function () {
                $('#mitabla').DataTable();
            } );
        </script>        
    </body>
</html>

